import json
from functools import partial
from romode.settings import SettingsWindow
from romode.download import on_download_button_clicked

import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk, GLib



class MainWindow(Gtk.ApplicationWindow):
    def __init__(self, config_file, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.config_file = config_file

        ### BASIC STUFF
        self.header = Gtk.HeaderBar()
        self.set_titlebar(self.header)
        self.set_resizable(False)
        self.set_default_size(600, 500)
        self.set_title("RoModeV3")

        self.preferences_button = Gtk.Button()
        self.preferences_button.set_icon_name("preferences-system-symbolic")
        self.preferences_button.valign = Gtk.Align.CENTER
        self.preferences_button.tooltip_text = "Preferences"
        self.preferences_button.connect("clicked", self.on_settings_button_clicked)    
        self.header.pack_end(child=self.preferences_button)



        ### WINDOW ELEMENTS
        self.mainbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.set_child(self.mainbox)
        
        self.input_vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)
        self.input_vbox.set_spacing(10)
        self.input_vbox.set_margin_top(10)
        self.input_vbox.set_margin_bottom(10)
        self.input_vbox.set_margin_start(10)
        self.input_vbox.set_margin_end(10)
        self.mainbox.append(self.input_vbox)

        self.youtube_link_label = Gtk.Label(label="Youtube Link*")
        self.youtube_link_label.set_xalign(0.0)
        self.input_vbox.append(self.youtube_link_label)

        self.youtube_link_entry = Gtk.Entry()
        self.youtube_link_entry.set_hexpand(True)
        self.youtube_link_entry.set_size_request(-1, 40)
        self.input_vbox.append(self.youtube_link_entry)

        self.spotify_link_label = Gtk.Label(label="Spotify Link (optional)")
        self.spotify_link_label.set_xalign(0.0)
        self.input_vbox.append(self.spotify_link_label)

        self.spotify_link_entry = Gtk.Entry()
        self.spotify_link_entry.set_size_request(580, 40)
        self.input_vbox.append(self.spotify_link_entry)


        self.buttons_hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
        self.buttons_hbox.set_spacing(10)
        self.buttons_hbox.set_margin_top(10)
        self.buttons_hbox.set_margin_bottom(10)
        self.buttons_hbox.set_margin_start(10)
        self.buttons_hbox.set_margin_end(10)
        self.mainbox.append(self.buttons_hbox)

        self.download_button = Gtk.Button(label="Download")
        self.download_button.set_hexpand(True)
        self.download_button.set_size_request(-1, 50)
        self.download_button.connect("clicked", partial(on_download_button_clicked, self, self.config_file, self.youtube_link_entry, self.spotify_link_entry))




        self.folder_choose_button = Gtk.Button(label="Choose Folder")
        self.folder_choose_button.connect("clicked", self.on_folder_choose_button_clicked)    

        self.folder_choose_button.set_hexpand(True)
        self.folder_choose_button.set_size_request(-1, 50)

        self.buttons_hbox.append(self.download_button);
        self.buttons_hbox.append(self.folder_choose_button);


        self.separator = Gtk.Separator(orientation=Gtk.Orientation.VERTICAL)
        self.separator.set_size_request(5, -1)
        self.mainbox.append(self.separator)


        self.scrolled_window_hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
        self.scrolled_window_hbox.set_spacing(10)
        self.scrolled_window_hbox.set_margin_bottom(10)
        self.scrolled_window_hbox.set_margin_start(10)
        self.scrolled_window_hbox.set_margin_end(10)
        self.mainbox.append(self.scrolled_window_hbox)

        self.scrolled_window = Gtk.ScrolledWindow()
        self.scrolled_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        self.scrolled_window_hbox.append(self.scrolled_window)
        self.adjustment = self.scrolled_window.get_vadjustment()

        self.text_view = Gtk.TextView()
        self.text_view.set_editable(False)
        self.text_view.set_wrap_mode(Gtk.WrapMode.WORD);
        self.text_view.set_cursor_visible(False)
        self.text_view.set_hexpand(True)
        self.text_view.set_vexpand(True)
        self.scrolled_window.set_child(self.text_view)
        self.textbuffer = self.text_view.get_buffer()
        self.timer_id = None

    ## Actual Functional 100% confirmed functions
    def on_settings_button_clicked(self, button):
        settings_window = SettingsWindow(self)
        settings_window.present()


    def on_folder_choose_button_clicked(self, button):
        file_dialog = Gtk.FileDialog()
        file_dialog.select_folder(button.get_root(), None, partial(on_folder_dialog_response, self))


def on_folder_dialog_response(self, dialog, result):
    try:
        folder = dialog.select_folder_finish(result)
        if folder:
            try:
                with open(self.config_file, 'r') as f:
                    cfg_data = json.load(f)
            except:
                cfg_data = {}

            cfg_data['folder_path'] = folder.get_path()

            with open(self.config_file, 'w') as f:
                json.dump(cfg_data, f, indent=4)

    except GLib.Error as e:
        print("[ERR] Error getting folder: ", e)