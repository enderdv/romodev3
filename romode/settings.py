import os
import json
from functools import partial

import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk


class SettingsWindow(Gtk.Window):
    def __init__(self, parent_window):
        try:
            if not os.path.exists(parent_window.config_file):
                print("[LOG] No config file found!\n    >>>Creating a new file at location: `" + parent_window.config_file + "`\n")
                with open(parent_window.config_file, 'w') as file:
                    pass
        
        except Exception as e:
            print("[ERR] Unable to create config file!\n    >>>Error: ", e)
            print("")
            return


        ### BASIC STUFF
        super().__init__(title="Settings")
        self.settings_header = Gtk.HeaderBar()
        self.set_titlebar(self.settings_header)
        self.set_resizable(False)
        self.set_transient_for(parent_window)
        self.set_modal(True)
        self.set_default_size(400, 300)



        ### WINDOW ELEMENTS
        mainbox = Gtk.Box.new(orientation=Gtk.Orientation.VERTICAL, spacing=12)
        mainbox.set_margin_top(margin=12)
        mainbox.set_margin_end(margin=12)
        mainbox.set_margin_bottom(margin=12)
        mainbox.set_margin_start(margin=12)
        self.set_child(child=mainbox)


        spotify_label = Gtk.Label()
        spotify_label.set_markup("<span size='20000'>Spotify</span>")
        spotify_label.set_halign(Gtk.Align.START)
        mainbox.append(spotify_label)

        id_entry_label = Gtk.Label(label="Spotify Client ID")
        id_entry_label.set_halign(Gtk.Align.START)
        mainbox.append(id_entry_label)

        id_entry = Gtk.Entry()
        id_entry.set_hexpand(True)
        id_entry.set_size_request(-1, 40)


        with open(parent_window.config_file, 'r') as f:
            try:
                cfg = json.load(f)
                spotify_client_id = cfg.get('spotify_client_id', "")
            except:
                spotify_client_id = ""

        if spotify_client_id == "":
            id_entry.set_placeholder_text("Enter your Spotify Client ID here")
        else:
            id_entry.set_text(spotify_client_id)
        
        id_entry.connect("changed", partial(on_id_entry_changed, parent_window.config_file))
        mainbox.append(id_entry)



        secret_entry_label = Gtk.Label(label="Spotify Client Secret")
        secret_entry_label.set_halign(Gtk.Align.START)
        mainbox.append(secret_entry_label)

        secret_entry = Gtk.Entry()
        secret_entry.set_hexpand(True)
        secret_entry.set_size_request(-1, 40)


        with open(parent_window.config_file, 'r') as f:
            try:
                cfg = json.load(f)
                spotify_client_secret = cfg.get('spotify_client_secret', "")
            except:
                spotify_client_secret = ""

        if spotify_client_secret == "":
            secret_entry.set_placeholder_text("Enter your Spotify Client Secret here")
        else:
            secret_entry.set_text(spotify_client_secret)

        secret_entry.connect("changed", partial(on_secret_entry_changed, parent_window.config_file))
        mainbox.append(secret_entry)



def on_id_entry_changed(config_file, entry):
    text = entry.get_text()

    if text == "":
        entry.set_placeholder_text("Enter your Spotify Client ID here")

    try:
        with open(config_file, 'r') as f:
            cfg_data = json.load(f)
    except:
        cfg_data = {}

    cfg_data['spotify_client_id'] = text

    with open(config_file, 'w') as f:
        json.dump(cfg_data, f, indent=4)



def on_secret_entry_changed(config_file, entry):
    text = entry.get_text()
    if text == "":
        entry.set_placeholder_text("Enter your Spotify Client Secret here")

    try:
        with open(config_file, 'r') as f:
            cfg_data = json.load(f)
    except:
        cfg_data = {}

    cfg_data['spotify_client_secret'] = text

    with open(config_file, 'w') as f:
        json.dump(cfg_data, f, indent=4)