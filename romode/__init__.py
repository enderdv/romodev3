import sys
import signal


### Catches SIGINT signal, closes the app and sends a message
def signal_handler(sig, frame):
    print("\nCaught interrupt (did you press Ctrl+C?)")
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)